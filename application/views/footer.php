   <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>Gole Village Çan 
                            <br>Çanakkale / TURKEY</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>My Social Accounts</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/erbekmetin" target="_blank" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/u/0/104447280310282795625" target="_blank" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/metinerbek" target="_blank" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/metin-erbek-3582b04b" target="_blank" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="http://bitbucket.com/metinerbek" target="_blank" class="btn-social btn-outline"><i class="fa fa-fw fa-bitbucket"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Contact</h3>
                        <i class="fa fa-envelope"></i> &nbsp; erbekmetin@gmail.com<br>
							<i class="fa fa-skype"></i> &nbsp; metin.erbek
						
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Metin ERBEK 2016 / Makasih Bootstrap :)
                    </div>
                </div>
            </div>
        </div>
    </footer>

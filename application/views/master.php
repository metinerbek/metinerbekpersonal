<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="About Metin Erbek">
    <meta name="author" content="Metin ERBEK">
	<meta name="robots" content="index,follow" />
	<link rel="icon" href="<?php echo base_url();?>favicon.ico">
	<meta name="keywords" content="metin erbek,metin,erbek,metinerbek, metin erbek çomü,çanakkale,turkish developer,developers,php developers,php yazılımcı,yazilimci,developer,codeigniter developer,php site,site maker,innovation,metin erbek kimdir">
    <title><?php 
	if(isset($title)){
		echo $title;
	}else{
		echo "Metin ERBEK's Personal Website";
	}
	
	
	?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	<script type="text/javascript">
			var site="<?php echo site_url(); ?>";
			</script>
	<?php 
	if(isset($js_files)){
		foreach($js_files as $file){
			?>
	<script type="text/javascript" src="<?php echo $file; ?>"></script>
			<?php
		}
	}
	
	?>
	<?php 
	if(isset($css_files)){
		foreach($css_files as $file){
			?>
	<link rel="stylesheet" type="text/css" href="<?php echo $file; ?>">
			<?php
		}
	}
	
	?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

	<?php $this->load->view("menu");?>

	<?php echo $content;?>

	<?php $this->load->view("footer");?>
 
    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <?php 
   if(isset($dialogs)){
		foreach($dialogs as $dialog){
			$this->load->view($dialog);
		}
	   
   }
   
   ?>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url();?>js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url();?>js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="<?php echo base_url();?>js/freelancer.min.js"></script>

</body>

</html>

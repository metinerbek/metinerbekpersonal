    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>EgitimKursSeminer.com</h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url();?>img/portfolio/eks.png" class="img-responsive img-centered" alt="">
                            <p><a href="https://egitimkursseminer.com" target="_blank">EgitimKursSeminer.com</a> for meeting with trainers and traniee companies or people. Has a control panel, user panels, Common pages etc. Working half multi language.i made this project for <a href="http://aksys.com.tr" target="_blank">AKSYS Group Ltd. company</a></p>
                            <ul class="list-inline item-details">
                                <li>What i did? :
                                    <strong>All Project,design,backend coding.
                                    </strong>
                                </li>
                                <li>Development Time:
                                    <strong>2013-2016 ~ Continue..
                                    </strong>
                                </li>
                                <li>Used Technologies:
                                    <strong>Codeigniter Framework, PHP, JQuery, Bootstrap
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Sutedarik.com</h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url();?>img/portfolio/sutedarik.png" class="img-responsive img-centered" alt="">
                            <p><a href="http://sutedarik.com" target="_blank">Sutedarik.com</a> for water distribution in Turkey. i write on web. i prepared <b>JSON webservices</b> also. Only Turkish.i made this project for <a href="http://aksys.com.tr" target="_blank">AKSYS Group Ltd. company</a></p>
                              <ul class="list-inline item-details">
                                <li>What i did? :
                                    <strong>Backend Coding
                                    </strong>
                                </li>
                                <li>Development Time:
                                    <strong>2013-2015
                                    </strong>
                                </li>
                                <li>Used Technologies:
                                    <strong>Codeigniter Framework, PHP, JQuery, Bootstrap, JSON
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Envaye</h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url();?>img/portfolio/envaye.png" class="img-responsive img-centered" alt="">
                            <p><a href="http://envaye.com">Envaye.com</a> for sharing all experiences about life. it was my first project with PHP.i tried innovational something.</p>
                            <ul class="list-inline item-details">
                                <li>What i did? :
                                    <strong>All Project,design,backend coding.
                                    </strong>
                                </li>
                                <li>Development Time:
                                    <strong>2008
                                    </strong>
                                </li>
                                <li>Used Technologies:
                                    <strong>PHP,HTML
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Ygs Konuları</h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url();?>img/portfolio/ygskonulari.png" class="img-responsive img-centered" alt="">
                            <p><a href="http://ygskonulari.envaye.com" target="_blank">YGSKonulari Project</a> content about university test subjects. Has small admin panel also.</p>
                            <ul class="list-inline item-details">
                                <li>What i did? :
                                    <strong>All Project,design,backend coding.
                                    </strong>
                                </li>
                                <li>Development Time:
                                    <strong>2014
                                    </strong>
                                </li>
                                <li>Used Technologies:
                                    <strong>Codeigniter Framework, PHP, JQuery, Metro UI
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Whyki</h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url();?>img/portfolio/whyki.png" class="img-responsive img-centered" alt="">
                            <p><a href="https://play.google.com/store/apps/details?id=why.ki.whyki" target="_blank">WhyKi</a> is an Android Puzzle game. I made for only try android project. i made another android projects for my worked company also but i didnt published.</p>
                            <ul class="list-inline item-details">
                                <li>What i did? :
                                    <strong>All Project,design,backend coding.
                                    </strong>
                                </li>
                                <li>Development Time:
                                    <strong>2014
                                    </strong>
                                </li>
                                <li>Used Technologies:
                                    <strong>Java, Android
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>My GF's Blog</h2>
                            <hr class="star-primary">
                            <img src="<?php echo base_url();?>img/portfolio/blog.png" class="img-responsive img-centered" alt="">
                            <p><a href="http://via.envaye.com" target="_blank">My Girlfriend's Blog</a> is a wordpress blog.I carried this blog from blogspot blog system. I can work with SMF Forum System, Wordpress [Editing], vBullettin Forum System</p>
                            <ul class="list-inline item-details">
                            <ul class="list-inline item-details">
                                <li>What i did? :
                                    <strong>Install Wordpress, install theme, Carry Blogspot
                                    </strong>
                                </li>
                                <li>Development Time:
                                    <strong>2016
                                    </strong>
                                </li>
                                <li>Used Technologies:
                                    <strong>Wordpress, PHP, Mysql
                                    </strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>